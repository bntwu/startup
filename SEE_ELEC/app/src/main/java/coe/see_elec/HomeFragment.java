package coe.see_elec;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View view;
    private TextView textPowerfactor, textReactivePower, textRealPower;
    private String[] value;
    private OnFragmentInteractionListener mListener;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        textPowerfactor = (TextView) view.findViewById(R.id.powerfactor);
        textReactivePower = (TextView) view.findViewById(R.id.reactivpower);
        textRealPower = (TextView) view.findViewById(R.id.realpower);
        value = new String[3];
        for(int i=0;i<3;i++){
            value[0] = new String();
        }
        try {
            new SimpleTask().execute();
            final Handler handler = new Handler();
            new Thread(new Runnable() {
                public void run() {
                    while (true) {
                        try{
                            Thread.sleep(10000);
                        }catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        try {
                            handler.post(new Runnable() {
                                public void run() {
                                    new SimpleTask().execute();
                                }
                            });
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }).start();
        }catch (Exception e){
            e.printStackTrace();
        }
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private String getWheelchair() {
        HttpsURLConnection urlConnection = null;
        StringBuilder result = new StringBuilder();

        try{
            URL url = new URL("https://thingspeak.com/channels/267563/feed.json");
            urlConnection = (HttpsURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String line;
            while((line = reader.readLine()) != null){
                result.append(line);
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        finally {
            urlConnection.disconnect();
        }

        try{
            JSONObject reader = new JSONObject(result.toString());
            JSONArray jsonArray = reader.optJSONArray("feeds");
            JSONObject jsonObject = jsonArray.getJSONObject(jsonArray.length()-1);
            value[0] = jsonObject.getString("field1").toString();
            value[1] = jsonObject.getString("field2").toString();
            value[2] = jsonObject.getString("field3").toString();

            return value[0];
        }catch(JSONException e){
            e.printStackTrace();
        }
        return "unsucces";
    }

    private class SimpleTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar

        }

        protected String doInBackground(String... urls) {
            String result = getWheelchair();
            publishProgress(result);
            return result;
        }

        protected void onPostExecute(String result) {
//            textPowerfactor.setText(value[0]+" Watt");
            textReactivePower.setText(value[1]+" Var");
            textRealPower.setText(value[2]);
//            textPowerfactor.setText("100 Watt");
//            textReactivePower.setText("20 Var");
//            textRealPower.setText("50");
        }
    }
}
