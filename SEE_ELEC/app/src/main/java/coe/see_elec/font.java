package coe.see_elec;

import android.app.Application;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Raspberry-PC on 9/17/2017.
 */

public class font extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        initFont();
    }

    private void initFont(){
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("font/THSarabunNew Bold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}
