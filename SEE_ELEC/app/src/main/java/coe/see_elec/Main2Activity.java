package coe.see_elec;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Main2Activity extends AppCompatActivity {
    private static final String my_shared = "shared_IP";
    int backButtonCount = 0;
    IP ip = new IP();
    String ip_address;
    InputMethodManager inputMethodManager;
    Pinview pinview;
    SharedPreferences sharedIP;
    private SharedPreferences.Editor editor;
    Button btn_setting;
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
//        sharedIP = getSharedPreferences(my_shared, Context.MODE_PRIVATE);
//        ip_address = sharedIP.getString("IP","");
//        btn_setting = (Button) findViewById(R.id.btn_settingIP);

//        if(ip_address.equals("")){
//            setDialog();
//            Toast.makeText(getApplicationContext(),ip_address.toString(),Toast.LENGTH_SHORT).show();
//        }else{
//            Toast.makeText(getApplicationContext(),ip_address.toString(),Toast.LENGTH_SHORT).show();
//        }
//
//        btn_setting.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                setDialog();
//            }
//        });

        pinview = (Pinview) findViewById(R.id.pinView);
        inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        pinview.requestFocus();
        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean b) {
//                resultPin.toString()
                if (pinview.getValue().toString().compareTo("1234") == 0) {
                    Intent i = new Intent(getApplicationContext(), BaseActivity.class);
                    startActivity(i);
                }
            }
        });
    }



    private void setDialog(){
        final Dialog dialog = new Dialog(Main2Activity.this);
        dialog.setTitle("SEEELEC");
        dialog.setContentView(R.layout.dialog_ip);
        final EditText ip = (EditText) dialog.findViewById(R.id.ip);
        Button buttonCancel = (Button) dialog.findViewById(R.id.button_cancel);
        Button buttonLogin = (Button) dialog.findViewById(R.id.button_accept);

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Check username password
                if (ip.getText().equals("")) {
                    Toast.makeText(getApplicationContext(), "กรุณาใส่หมายเลข IP", Toast.LENGTH_SHORT).show();
                }else{
                    editor = sharedIP.edit();
                    editor.putString("IP",ip.getText().toString());
                    ip_address = ip.getText().toString();
                    editor.commit();
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        backButtonCount = 0;
//        inputMethodManager.showSoftInput(pinview, InputMethodManager.SHOW_IMPLICIT);
    }

    @Override
    protected void onPause() {
        super.onPause();
        inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    @Override
    public void onBackPressed() {
        if (backButtonCount >= 1) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Toast.makeText(this, "กดอีกครั้ง หากต้องการปิด", Toast.LENGTH_SHORT).show();
            backButtonCount++;
        }
    }


}
