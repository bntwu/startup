package coe.see_elec;

import android.app.Activity;
import android.content.Context;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BaseActivity extends AppCompatActivity {
    FragmentManager fragmentManager;
    FragmentTransaction transaction;
    HomeFragment homeFragment;
    BottomNavigationView bottomNavigationView;
    InputMethodManager inputMethodManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        fragmentManager = getSupportFragmentManager();

        transaction= fragmentManager.beginTransaction();
        homeFragment = new HomeFragment();
        transaction.replace(R.id.contentPanel, homeFragment).addToBackStack("null").commit();
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_view);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                fragmentManager = getSupportFragmentManager();
                transaction= fragmentManager.beginTransaction();
                switch (item.getItemId()){
                    case R.id.item_home:
                        transaction.replace(R.id.contentPanel, homeFragment).commit();
                        item.setChecked(true);
                        break;

                    case R.id.item_bar:
                        transaction.replace(R.id.contentPanel, new HistoryFragment()).commit();
                        item.setChecked(true);
                        break;

                    case R.id.item_setting:
                        transaction.replace(R.id.contentPanel, new SettingFragment()).commit();
                        item.setChecked(true);
                        break;
                }
                return false;
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0 ) {
            getSupportFragmentManager().popBackStack();
        }
    }
}
