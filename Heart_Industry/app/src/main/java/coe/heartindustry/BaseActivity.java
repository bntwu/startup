package coe.heartindustry;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.ActionMenuItem;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class BaseActivity extends AppCompatActivity {
    private IP ip = new IP();
    InputMethodManager inputMethodManager;
    BottomNavigationView bottomNavigationView;
    FragmentManager fragmentManager;
    FragmentTransaction transaction;
    Intent intent;
    String customer_id="";
    Bundle bundleFragment = new Bundle();
    HomeFragment homeFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        fragmentManager = getSupportFragmentManager();
        transaction= fragmentManager.beginTransaction();
        homeFragment = new HomeFragment();
        try{
            Bundle bundle = getIntent().getExtras();
            customer_id = bundle.getString("customer_id");
            bundleFragment.putString("customer_id",customer_id.toString());
            homeFragment.setArguments(bundleFragment);
            transaction.replace(R.id.contentPanel, homeFragment).addToBackStack("null").commit();
        }catch (Exception e){
            e.printStackTrace();
        }

        inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_view);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                fragmentManager = getSupportFragmentManager();
                transaction= fragmentManager.beginTransaction();
                switch (item.getItemId()){
                    case R.id.item_home:
                        transaction.replace(R.id.contentPanel, homeFragment).commit();
                        item.setChecked(true);
                        break;
                    case R.id.item_qr:
                        transaction.replace(R.id.contentPanel, new QrFragment()).commit();
                        item.setChecked(true);
                        break;
                    case R.id.item_setting:
                        transaction.replace(R.id.contentPanel, new SettingFragment()).commit();
                        item.setChecked(true);
                        break;
                }
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0 ) {
            getSupportFragmentManager().popBackStack();
        }
    }

}
