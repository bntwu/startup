package coe.heartindustry;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Raspberry-PC on 8/28/2017.
 */

public class Adapter_listView extends ArrayAdapter<data_motor>{
    private ViewHolder mViewHolder;

    public Adapter_listView(Context context, ArrayList<data_motor> data) {
        super(context, 0, data);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        data_motor data = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listview_customer_home, parent, false);
            mViewHolder = new ViewHolder();
            mViewHolder.img = (ImageView) convertView.findViewById(R.id.image);
            mViewHolder.text_name = (TextView) convertView.findViewById(R.id.text_name);
            mViewHolder.text_series = (TextView) convertView.findViewById(R.id.text_series);
            mViewHolder.text_type = (TextView) convertView.findViewById(R.id.text_type);
            mViewHolder.text_power = (TextView) convertView.findViewById(R.id.text_power);
            mViewHolder.text_horse = (TextView) convertView.findViewById(R.id.text_horse);

            mViewHolder.img.setImageResource(R.drawable.motor);
            if(data.getColor().equals("3")){
                mViewHolder.img.setBackground(convertView.getResources().getDrawable(R.drawable.red_round));
            }else{
                if(data.getColor().equals("2")){
                    mViewHolder.img.setBackground(convertView.getResources().getDrawable(R.drawable.yellow_round));
                }else{
                    if(data.getColor().equals("1")){
                        mViewHolder.img.setBackground(convertView.getResources().getDrawable(R.drawable.green_round));
                    }
                }
            }
            mViewHolder.text_name.setText(data.getName());
            mViewHolder.text_series.setText("  Machine ID: " + data.getSeries().toString());
            mViewHolder.text_type.setText("  Type: " + data.getType().toString());
            mViewHolder.text_power.setText("  Power: " + data.getPower().toString() + " kW");
            mViewHolder.text_horse.setText("  Horse Power: " + data.getHorse().toString() + " Hp");

            convertView.setTag(mViewHolder);
        }else{
            mViewHolder = (ViewHolder) convertView.getTag();
        }
        return convertView;
    }

    private static class ViewHolder{
        ImageView img;
        TextView text_name;
        TextView text_series;
        TextView text_type;
        TextView text_power;
        TextView text_horse;
    }
}
