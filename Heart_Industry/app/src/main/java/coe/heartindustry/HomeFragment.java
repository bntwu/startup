package coe.heartindustry;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {
    private IP ip = new IP();
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    OnFragmentSendText mSendText;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String my_shared = "shared_IP";
    private ListView listView;
    private View view;
    private Adapter_listView mAdapter;
    private ArrayList<data_motor> mArray;
    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;
    private Bundle bundle = new Bundle();
    private String[] data,motor_name,motor_type,motor_series,motor_id,motor_color;
    private Double[] value_temp,value_hum,value_vib,value_cir;
    private Double[] motor_power,motor_horse;
    private int green,yellow,red,count=0;
    private String customer_id="";
    private Double[] temp_defult,hum_defult,vib_defult,cir_defult;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    SharedPreferences sharedIP;
    String ip_addres="";


    private OnFragmentInteractionListener mListener;



    public interface OnFragmentSendText{
        public void onSendText(String text);
    }

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        Bundle bundle = this.getArguments();
        sharedIP = this.getActivity().getSharedPreferences(my_shared, Context.MODE_PRIVATE);
        temp_defult =  new Double[3];
        hum_defult = new Double[3];
        vib_defult = new Double[3];
        cir_defult = new Double[3];

        ip.ip = sharedIP.getString("IP","");
        try {
            customer_id = bundle.getString("customer_id");
            new SimpleTask().execute("http://"+ip.ip.toString() + "/heart_Industy/php_motor_header.php?id=" + customer_id.toString());
        }catch (Exception e){
            e.printStackTrace();
        }
        setHasOptionsMenu(true);
        return view;
    }

    private void setListView(){
        fragmentManager = getFragmentManager();
        transaction= fragmentManager.beginTransaction();
        mArray = new ArrayList<data_motor>();
        for(int i=0;i<count;i++){
            data_motor motor = new data_motor();
            motor.setColor(motor_color[i].toString());
            motor.setName(motor_name[i].toString());
            motor.setType(motor_type[i].toString());
            motor.setSeries(motor_series[i].toString());
            motor.setPower(motor_power[i]);
            motor.setHorse(motor_horse[i]);
            mArray.add(motor);
        }

        mAdapter = new Adapter_listView(getActivity(),mArray);
        listView = (ListView) view.findViewById(R.id.list_item);

        listView.setAdapter(mAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MotorFragment fragment;
                fragment = new MotorFragment();
                bundle.putString("name",motor_name[position].toString());
                bundle.putString("type",motor_type[position].toString());
                bundle.putString("id",motor_id[position].toString());
                fragment.setArguments(bundle);
                transaction.replace(R.id.contentPanel, fragment).commit();
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_home, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
//            Toast.makeText(context, "Home", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private class SimpleTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... urls) {

            String result="";
            try{
                HttpGet httpGet = new HttpGet(urls[0]);
                HttpClient client = new DefaultHttpClient();
                HttpResponse response = client.execute(httpGet);
                int statusCode = response.getStatusLine().getStatusCode();
                if(statusCode == 200){
                    InputStream inputStream = response.getEntity().getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                    String line;

                    while((line = reader.readLine()) != null){
                        result += line;
                    }

                }else{

                }
            }catch(ClientProtocolException e){

            }catch(IOException e){

            }
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            getResult(s);
        }
    }

    private void getResult(String jsonString){
        try {
            JSONArray jArray = new JSONArray(jsonString);
            count = jArray.length();
            motor_name = new String[count];
            motor_type = new String[count];
            motor_series = new String[count];
            motor_power = new Double[count];
            motor_horse = new Double[count];
            motor_id = new String[count];
            motor_color = new String[count];
            for(int i=0;i<count;i++){
                JSONObject jsonObject = jArray.getJSONObject(i);
                motor_name[i] = jsonObject.getString("motor_name").toString();
                motor_type[i] = jsonObject.getString("motor_type").toString();
                motor_series[i] = jsonObject.getString("motor_series").toString();
                motor_power[i] = Double.parseDouble(jsonObject.getString("motor_power").toString());
                motor_horse[i] = Double.parseDouble(jsonObject.getString("motor_horse").toString());
                motor_id[i] = jsonObject.getString("motor_id").toString();
            }

            new SimpleTask2().execute("http://"+ip.ip.toString() + "/heart_Industy/php_motor_detail.php");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private class SimpleTask2 extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... urls) {

            String result="";
            try{
                HttpGet httpGet = new HttpGet(urls[0]);
                HttpClient client = new DefaultHttpClient();
                HttpResponse response = client.execute(httpGet);
                int statusCode = response.getStatusLine().getStatusCode();
                if(statusCode == 200){
                    InputStream inputStream = response.getEntity().getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                    String line;

                    while((line = reader.readLine()) != null){
                        result += line;
                    }

                }else{

                }
            }catch(ClientProtocolException e){

            }catch(IOException e){

            }
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            getValue(s);
        }
    }

    private void getValue(String jsonString){
        try {
            JSONArray jArray = new JSONArray(jsonString);
            value_temp = new Double[jArray.length()];
            value_hum = new Double[jArray.length()];
            value_cir = new Double[jArray.length()];
            value_vib = new Double[jArray.length()];
            for(int i=0;i<jArray.length();i++){
                green = 0;
                red = 0;
                yellow = 0;
                JSONObject jsonObject = jArray.getJSONObject(i);
                value_temp[i] = Double.parseDouble(jsonObject.getString("temp").toString());
                value_hum[i] = Double.parseDouble(jsonObject.getString("hum").toString());
                value_vib[i] = Double.parseDouble(jsonObject.getString("vib").toString());
                value_cir[i] = Double.parseDouble(jsonObject.getString("circult").toString());
                setMotor(i);
            }
            setListView();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setMotor(int i){
        temp_defult[0] = 40.0;
        temp_defult[1] = 100.0;
        temp_defult[2] = 120.0;
        hum_defult[0] = 59.0;
        hum_defult[1] = 73.0;
        hum_defult[2] = 85.0;
        vib_defult[0] = 0.71;
        vib_defult[1] = 7.1;
        vib_defult[2] = 28.0;
        cir_defult[0] = 187.5;
        cir_defult[1] = 328.125;
        cir_defult[2] = 375.0;
        try {
            if (value_hum[i] < hum_defult[1]) {
                green++;
            } else {
                if (value_hum[i] < hum_defult[2]) {
                    yellow++;
                } else {
                    red++;
                }
            }

            if (value_temp[i] < temp_defult[1]) {
                green++;
            } else {
                if (value_temp[i] < temp_defult[2]) {
                    yellow++;
                } else {
                    red++;
                }
            }

            if (value_vib[i] < vib_defult[1]) {
                green++;
            } else {
                if (value_vib[i] < vib_defult[2]) {
                    yellow++;
                } else {
                    red++;
                }
            }

            if (value_cir[i] < cir_defult[1]) {
                green++;
            } else {
                if (value_cir[i] < cir_defult[2]) {
                    yellow++;
                } else {
                    red++;
                }
            }

            if(red > 0){
                motor_color[i] = "3";
            }else{
                if(yellow > 0){
                    motor_color[i] = "2";
                }else{
                    if(green > 0){
                        motor_color[i] = "1";
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
