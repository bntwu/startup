package coe.heartindustry;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    private static final int WELCOME_TIMEOUT = 2500;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Handler handler = new Handler();
        final int totalProgressBar = 100;

        new Thread(new Runnable() {
            int progress=0;
            public void run() {
                while (progress < totalProgressBar) {
                    progress += (int) (Math.random()*15+10);
                    try{
                        Thread.sleep(500);
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                    // Update the progress bar
                    handler.post(new Runnable() {
                        public void run() {
//                            mProgressBar.setProgress(progress);
//                            if(progress > 100){
//                                mTextViewPercentage.setText("" + "100" + "%");
//                            }else {
//                                mTextViewPercentage.setText("" + progress + "%");
//                            }
                        }
                    });
                }
            }
        }).start();

//        Intent i = new Intent(getApplicationContext(), ThingSpeakActivity.class);
        Intent i = new Intent(getApplicationContext(), Main2Activity.class);
        startActivity(i);
    }
}
