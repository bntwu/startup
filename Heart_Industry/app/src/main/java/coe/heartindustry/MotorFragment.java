package coe.heartindustry;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MotorFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MotorFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MotorFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private View view;
    // TODO: Rename and change types of parameters
    private static final String my_shared = "shared_IP";
    IP ip = new IP();
    private String mParam1;
    private String mParam2,power_horse="",motor_id="";
    private TextView text_name,text_type;
    private OnFragmentInteractionListener mListener;
    private ImageView image_motor,image_hum,image_vib,image_temp,image_circult;
    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;
    private ToggleButton toggle;
    private ProgressDialog progressDialog;
    Double[] temp_defult,hum_defult,vib_defult,cir_defult;
    Double temp,hum,vib,cir;
    private boolean on=true;
    private int color=0,green=0,yellow=0,red=0,color_hum,color_vib,color_temp,color_cir;
    private Bundle bundle = new Bundle();
    SharedPreferences sharedIP;
    public MotorFragment() {
        // Required empty public constructor
    }

    public static MotorFragment newInstance(String param1, String param2) {
        MotorFragment fragment = new MotorFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_motor, container, false);
        text_name = (TextView) view.findViewById(R.id.text_name);
        text_type = (TextView) view.findViewById(R.id.text_type);
        image_motor = (ImageView) view.findViewById(R.id.image_motor);
//        image_hum = (ImageView) view.findViewById(R.id.image_hum);
        image_vib = (ImageView) view.findViewById(R.id.image_vib);
        image_temp = (ImageView) view.findViewById(R.id.image_temp);
        image_circult = (ImageView) view.findViewById(R.id.image_cir);
        toggle = (ToggleButton) view.findViewById(R.id.toggle);
        toggle.setChecked(true);
        sharedIP = getContext().getSharedPreferences(my_shared, Context.MODE_PRIVATE);
        try {
            Bundle bundle = this.getArguments();
//            power_horse = bundle.getString("power_horse").toString();
            motor_id = bundle.getString("id").toString();
            text_name.setText(bundle.getString("name"));
            text_type.setText(bundle.getString("type"));
            temp_defult =  new Double[3];
//            hum_defult = new Double[3];
            vib_defult = new Double[3];
            cir_defult = new Double[3];
            setDefult();
            ip.ip = sharedIP.getString("IP","");
            new SimpleTask().execute("http://"+ip.ip.toString() + "/heart_Industy/php_motor_detail.php?motor_id=" + motor_id);
            final Handler handler = new Handler();
            new Thread(new Runnable() {
                public void run() {
                    while (true) {
                        try{
                            Thread.sleep(10000);
                        }catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        try {
                            handler.post(new Runnable() {
                                public void run() {
                                    new SimpleTask().execute("http://"+ip.ip.toString() + "/heart_Industy/php_motor_detail.php?motor_id=" + motor_id);
                                }
                            });
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }).start();


        }catch (Exception e){
            e.printStackTrace();
        }

        return view;
    }

    void setDefult(){
        temp_defult[0] = 40.0;
        temp_defult[1] = 100.0;
        temp_defult[2] = 120.0;
//        hum_defult[0] = 59.0;
//        hum_defult[1] = 73.0;
//        hum_defult[2] = 85.0;
        vib_defult[0] = 0.71;
        vib_defult[1] = 7.1;
        vib_defult[2] = 28.0;
        cir_defult[0] = 187.5;
        cir_defult[1] = 328.125;
        cir_defult[2] = 375.0;
        fragmentManager = getFragmentManager();
        transaction= fragmentManager.beginTransaction();
//        image_motor.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ChartMotor_Fragment fragment = new ChartMotor_Fragment();
//                transaction.replace(R.id.contentPanel, fragment).addToBackStack("tag").commit();
//            }
//        });

//        image_hum.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ChartHum_Fragment fragment = new ChartHum_Fragment();
//                bundle.putString("hum", hum.toString());
//                bundle.putInt("color",color_hum);
//                fragment.setArguments(bundle);
//                transaction.replace(R.id.contentPanel, fragment).addToBackStack("tag").commit();
//            }
//        });

        image_vib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChartVib_Fragment fragment = new ChartVib_Fragment();
                bundle.putString("vib", vib.toString());
                bundle.putInt("color",color_vib);
                fragment.setArguments(bundle);
                transaction.replace(R.id.contentPanel, fragment).addToBackStack("tag").commit();
            }
        });

        image_temp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChartTemp_Fragment fragment = new ChartTemp_Fragment();
                bundle.putString("temp", temp.toString());
                bundle.putInt("color",color_temp);
                fragment.setArguments(bundle);
                transaction.replace(R.id.contentPanel, fragment).addToBackStack("tag").commit();
            }
        });

        image_circult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChartCir_Fragment fragment = new ChartCir_Fragment();
                bundle.putString("cir", cir.toString());
                bundle.putInt("color",color_cir);
                fragment.setArguments(bundle);
                transaction.replace(R.id.contentPanel, fragment).addToBackStack("tag").commit();
            }
        });

        toggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(toggle.isChecked() == true){
                    toggle.setBackground(getResources().getDrawable(R.drawable.power_on));
                    on = true;
                    new SimpleTask2().execute("https://api.thingspeak.com/update?api_key=2JZX4BC93VR3DIVO");
                    setProgressDialog("On Motor");
//                    toggle.setText("");
                }else{
                    toggle.setBackground(getResources().getDrawable(R.drawable.power_off));
                    on = false;
                    new SimpleTask2().execute("https://api.thingspeak.com/update?api_key=2JZX4BC93VR3DIVO");
                    setProgressDialog("Off Motor");
//                    toggle.setText("");
                }
            }
        });


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class SimpleTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... urls) {

            String result="";
            try{
                HttpGet httpGet = new HttpGet(urls[0]);
                HttpClient client = new DefaultHttpClient();
                HttpResponse response = client.execute(httpGet);
                int statusCode = response.getStatusLine().getStatusCode();
                if(statusCode == 200){
                    InputStream inputStream = response.getEntity().getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                    String line;

                    while((line = reader.readLine()) != null){
                        result += line;
                    }

                }else{

                }
            }catch(ClientProtocolException e){

            }catch(IOException e){

            }
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            getResult(s);
        }
    }

//    private void getResult(String jsonString){
//        try {
//            JSONArray jArray = new JSONArray(jsonString);
//            JSONObject jsonObject = jArray.getJSONObject(0);
//            temp_defult[0] = Double.parseDouble(jsonObject.getString("t_green").toString());
//            temp_defult[1] = Double.parseDouble(jsonObject.getString("t_yellow").toString());
//            temp_defult[2] = Double.parseDouble(jsonObject.getString("t_red").toString());
//            hum_defult[0] = Double.parseDouble(jsonObject.getString("h_green").toString());
//            hum_defult[1] = Double.parseDouble(jsonObject.getString("h_yellow").toString());
//            hum_defult[2] = Double.parseDouble(jsonObject.getString("h_red").toString());
//            vib_defult[0] = Double.parseDouble(jsonObject.getString("v_green").toString());
//            vib_defult[1] = Double.parseDouble(jsonObject.getString("v_yellow").toString());
//            vib_defult[2] = Double.parseDouble(jsonObject.getString("v_red").toString());
//            cir_defult[0] = Double.parseDouble(jsonObject.getString("c_green").toString());
//            cir_defult[1] = Double.parseDouble(jsonObject.getString("c_yellow").toString());
//            cir_defult[2] = Double.parseDouble(jsonObject.getString("c_red").toString());
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }

    private void getResult(String jsonString){
            try {
                JSONArray jArray = new JSONArray(jsonString);

                JSONObject jsonObject = jArray.getJSONObject(0);
                temp = Double.parseDouble(jsonObject.getString("temp").toString());
//                hum = Double.parseDouble(jsonObject.getString("hum").toString());
                vib = Double.parseDouble(jsonObject.getString("vib").toString());
                cir = Double.parseDouble(jsonObject.getString("circult").toString());
                setImage();
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }

    private void setImage(){
        green = 0;
        red = 0;
        yellow = 0;
        try {
//            if (hum < hum_defult[1]) {
//                image_hum.setBackground(getResources().getDrawable(R.drawable.green_round));
//                color_hum = 1;
//                green++;
//            } else {
//                if (hum < hum_defult[2]) {
//                    image_hum.setBackground(getResources().getDrawable(R.drawable.yellow_round));
//                    color_hum = 2;
//                    yellow++;
//                } else {
//                    image_hum.setBackground(getResources().getDrawable(R.drawable.red_round));
//                    color_hum = 3;
//                    red++;
//                }
//            }

            if (temp < temp_defult[1]) {
                image_temp.setBackground(getResources().getDrawable(R.drawable.green_round));
                color_temp = 1;
                green++;
            } else {
                if (temp < temp_defult[2]) {
                    image_temp.setBackground(getResources().getDrawable(R.drawable.yellow_round));
                    color_temp = 2;
                    yellow++;
                } else {
                    image_temp.setBackground(getResources().getDrawable(R.drawable.red_round));
                    color_temp = 3;
                    red++;
                }
            }

            if (vib < vib_defult[1]) {
                image_vib.setBackground(getResources().getDrawable(R.drawable.green_round));
                color_vib = 1;
                green++;
            } else {
                if (vib < vib_defult[2]) {
                    image_vib.setBackground(getResources().getDrawable(R.drawable.yellow_round));
                    color_vib = 2;
                    yellow++;
                } else {
                    image_vib.setBackground(getResources().getDrawable(R.drawable.red_round));
                    color_vib = 3;
                    red++;
                }
            }

            if (cir < cir_defult[1]) {
                image_circult.setBackground(getResources().getDrawable(R.drawable.green_round));
                color_cir = 1;
                green++;
            } else {
                if (cir < cir_defult[2]) {
                    image_circult.setBackground(getResources().getDrawable(R.drawable.yellow_round));
                    color_cir = 2;
                    yellow++;
                } else {
                    image_circult.setBackground(getResources().getDrawable(R.drawable.red_round));
                    color_cir = 3;
                    red++;
                }
            }

            if(red > 0){
                image_motor.setBackground(getResources().getDrawable(R.drawable.red_round));
            }else{
                if(yellow > 0){
                    image_motor.setBackground(getResources().getDrawable(R.drawable.yellow_round));
                }else{
                    if(green > 0){
                        image_motor.setBackground(getResources().getDrawable(R.drawable.green_round));
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private class SimpleTask2 extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... urls) {

            String result="";
            try{
                HttpPost httpPost = new HttpPost(urls[0]);
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                if(on == true){
                    params.add(new BasicNameValuePair("field2","1"));
                }else{
                    params.add(new BasicNameValuePair("field2","0"));
                }

                UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params, "UTF-8");
                httpPost.setEntity(ent);

                HttpClient client = new DefaultHttpClient();
                HttpResponse response = client.execute(httpPost);
            }catch(ClientProtocolException e){

            }catch(IOException e){

            }
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
//            getResult(s);
            Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();

        }
    }

    private void setProgressDialog(String msg){
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMax(100);
        progressDialog.setMessage("Wait...");
        progressDialog.setTitle(msg);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        final Handler handler = new Handler();
        final int totalProgress = 100;
        new Thread(new Runnable() {
            int progress=0;
            @Override
            public void run() {
                while (progress <= totalProgress){
                    try{
                            progress += (int) (Math.random()*15+10);
                            Thread.sleep(500);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    handler.post(new Runnable() {
                        public void run() {
                        progressDialog.setProgress(progress);
                        }
                    });
                }
                progressDialog.dismiss();
            }
        }).start();;
    }
}
