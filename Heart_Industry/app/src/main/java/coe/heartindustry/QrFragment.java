package coe.heartindustry;

import android.*;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

//import com.google.android.gms.vision.CameraSource;
//import com.google.android.gms.vision.Detector;
//import com.google.android.gms.vision.barcode.Barcode;
//import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link QrFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link QrFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class QrFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    SurfaceView cameraPreview;
    TextView txtResult;
    private View view;
//    BarcodeDetector barcodeDetector;
//    CameraSource cameraSource;
    final int RequestCameraPermissionID = 1001;
    GradientDrawable drawable;
    private volatile boolean running = true;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public QrFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment QrFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static QrFragment newInstance(String param1, String param2) {
        QrFragment fragment = new QrFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_qr, container, false);
//        cameraPreview = (SurfaceView) view.findViewById(R.id.cameraPreview);
//        drawable = new GradientDrawable();
//        drawable.setShape(GradientDrawable.RECTANGLE);
//        drawable.setStroke(10, Color.RED);
//        cameraPreview.setBackgroundDrawable(drawable);
//        cameraPreview.setPadding(10,10,10,10);
//
//        txtResult = (TextView) view.findViewById(R.id.txtResult);
//
//        barcodeDetector = new BarcodeDetector.Builder(getContext())
//                .setBarcodeFormats(Barcode.QR_CODE)
//                .build();
//
//        cameraSource = new CameraSource
//                .Builder(getContext(), barcodeDetector)
//                .setRequestedPreviewSize(640,480)
//                .setAutoFocusEnabled(true)
//                .build();
//
//        cameraPreview.getHolder().addCallback(new SurfaceHolder.Callback() {
//            @Override
//            public void surfaceCreated(SurfaceHolder holder) {
//                if(ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
//                    ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.CAMERA}, RequestCameraPermissionID);
//                    return;
//                }
//                try {
//                    cameraSource.start(cameraPreview.getHolder());
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
//
//            }
//
//            @Override
//            public void surfaceDestroyed(SurfaceHolder holder) {
//                cameraSource.stop();
//            }
//        });
//
//        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
//            @Override
//            public void release() {
//
//            }
//
//            @Override
//            public void receiveDetections(Detector.Detections<Barcode> detections) {
//                final SparseArray<Barcode> qrcodes = detections.getDetectedItems();
//                if(qrcodes.size() != 0){
//
//                    txtResult.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            while(running) {
//                                try {
//                                    Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
//                                    vibrator.vibrate(1000);
//                                    drawable.setStroke(10, Color.GREEN);
//                                    cameraPreview.setBackgroundDrawable(drawable);
//                                    setDialog(qrcodes);
//                                    Thread.sleep(1000);
//                                }catch (InterruptedException e) {
//                                    e.printStackTrace();
//                                }
//
//                                if (qrcodes.valueAt(0).displayValue != null) {
//                                    running = false;
//                                }
//                            }
//
//                        }
//
//                    });
//                }
//            }
//        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

//    private void setDialog(final SparseArray<Barcode> qrcodes){
//        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//        builder.setTitle("ต้องการเพิ่มเครื่องซักผ้าหรือไหม");
//        builder.setMessage("ID: "+qrcodes.valueAt(0).displayValue);
//        builder.setPositiveButton("ยืนยัน", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//        });
//
//        builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//        });
//        builder.create();
//        builder.show();
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
//            Toast.makeText(context, "QR Code", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
