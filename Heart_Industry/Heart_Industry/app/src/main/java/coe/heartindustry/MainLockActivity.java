package coe.heartindustry;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.nightonke.blurlockview.BlurLockView;
import com.nightonke.blurlockview.Directions.HideType;
import com.nightonke.blurlockview.Directions.ShowType;
import com.nightonke.blurlockview.Eases.EaseType;
import com.nightonke.blurlockview.Password;

public class MainLockActivity extends AppCompatActivity {
    private BlurLockView blurLockView;
    private ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_lock);

        blurLockView = (BlurLockView) findViewById(R.id.blurLock_views);


        imageView = (ImageView) findViewById(R.id.image_back);
        blurLockView.setBlurredView(imageView);
        blurLockView.setCorrectPassword("1234");
        blurLockView.setLeftButton(" ");
        blurLockView.setSmallButtonViewsBackground(R.drawable.blurlock);
//        blurLockView.setBlurRadius(R.drawable.blurlock);
//        blurLockView.setBigButtonViewsBackground(R.drawable.blurlock);
        blurLockView.setRightButton("Delete");
        blurLockView.setTypeface(Typeface.DEFAULT);
        blurLockView.setType(Password.NUMBER,false);

        blurLockView.setOnPasswordInputListener(new BlurLockView.OnPasswordInputListener() {
            @Override
            public void correct(String inputPassword) {
                Toast.makeText(getApplicationContext(), "Password Correct", Toast.LENGTH_LONG).show();
                blurLockView.hide(1000, HideType.FADE_OUT, EaseType.EaseInBounce);
                Intent i = new Intent(getApplicationContext(), BaseActivity.class);
                startActivity(i);
            }

            @Override
            public void incorrect(String inputPassword) {
                Toast.makeText(getApplicationContext(), "Password Incorrect", Toast.LENGTH_LONG).show();
            }

            @Override
            public void input(String inputPassword) {
                //
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                blurLockView.show(1000, ShowType.FADE_IN, EaseType.EaseInOutBack);
            }
        });
    }
}
