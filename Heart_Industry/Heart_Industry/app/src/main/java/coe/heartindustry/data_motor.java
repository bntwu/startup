package coe.heartindustry;

/**
 * Created by Raspberry-PC on 8/28/2017.
 */

public class data_motor {
    private String no;
    private String name;
    private Double temp;
    private String type;
    private Double power,horse;
    private String series;
    private String color;

    public Double getPower() {
        return power;
    }

    public void setPower(Double power) {
        this.power = power;
    }

    public Double getHorse() {
        return horse;
    }

    public void setHorse(Double horse) {
        this.horse = horse;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getTemp() {
        return temp;
    }

    public void setTemp(Double temp) {
        this.temp = temp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
