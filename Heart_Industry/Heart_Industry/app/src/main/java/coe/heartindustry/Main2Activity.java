package coe.heartindustry;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Main2Activity extends AppCompatActivity {
    IP ip = new IP();
    InputMethodManager inputMethodManager;
    int backButtonCount = 0;
    Pinview pinview;
    final Handler handler = new Handler();
    String resultPin="",customer_id="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        new SimpleTask().execute( ip.ip.toString() + "heart_Industy/php_customer.php?user=admin");
        pinview = (Pinview) findViewById(R.id.pinView);
        inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        pinview.requestFocus();
        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean b) {
//                resultPin.toString()
                if (pinview.getValue().toString().compareTo("1234") == 0) {
                    Intent i = new Intent(getApplicationContext(), BaseActivity.class);
                    i.putExtra("customer_id",customer_id);
                    startActivity(i);
                }
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        backButtonCount = 0;
        inputMethodManager.showSoftInput(pinview,InputMethodManager.SHOW_IMPLICIT);
    }

    @Override
    protected void onPause() {
        super.onPause();
        inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    @Override
    public void onBackPressed() {
        if (backButtonCount >= 1) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Toast.makeText(this, "กดอีกครั้ง หากต้องการปิด", Toast.LENGTH_SHORT).show();
            backButtonCount++;
        }
    }

    private class SimpleTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... urls) {

            String result="";
            try{
                HttpGet httpGet = new HttpGet(urls[0]);
                HttpClient client = new DefaultHttpClient();
                HttpResponse response = client.execute(httpGet);
                int statusCode = response.getStatusLine().getStatusCode();
                if(statusCode == 200){
                    InputStream inputStream = response.getEntity().getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                    String line;

                    while((line = reader.readLine()) != null){
                        result += line;
                    }

                }else{

                }
            }catch(ClientProtocolException e){

            }catch(IOException e){

            }
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            getResult(s);
        }
    }

    private void getResult(String jsonString){
        try {
            JSONArray jArray = new JSONArray(jsonString);
            JSONObject jsonObject = jArray.getJSONObject(0);
            resultPin = jsonObject.getString("Customer_pin").toString();
            customer_id = jsonObject.getString("Customer_id").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
