package coe.heartindustry;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.macroyau.thingspeakandroid.ThingSpeakChannel;
import com.macroyau.thingspeakandroid.ThingSpeakLineChart;
import com.macroyau.thingspeakandroid.model.ChannelFeed;

import java.util.Calendar;

import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.view.LineChartView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ChartHum_Fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ChartHum_Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChartHum_Fragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private View view;
    private LineChartView lineChartView;
    private ThingSpeakChannel tsChannel;
    private ThingSpeakLineChart tsChart;
    private String hum="";
    private TextView text_value;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private Calendar calendar;
    public ChartHum_Fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ChartHum_Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChartHum_Fragment newInstance(String param1, String param2) {
        ChartHum_Fragment fragment = new ChartHum_Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_chart_hum_, container, false);
        lineChartView = (LineChartView) view.findViewById(R.id.chart);
        try{
            Bundle bundle = this.getArguments();
            hum = bundle.getString("hum");

            int color = bundle.getInt("color");
            ImageView image_hum = (ImageView) view.findViewById(R.id.image_hum);
            if(color == 1){
                image_hum.setBackground(getResources().getDrawable(R.drawable.green_round));
            }else{
                if(color == 2){
                    image_hum.setBackground(getResources().getDrawable(R.drawable.yellow_round));
                }else{
                    if(color == 3){
                        image_hum.setBackground(getResources().getDrawable(R.drawable.red_round));
                    }
                }
            }

            text_value = (TextView) view.findViewById(R.id.text_value);
            text_value.setText(hum);


        }catch (Exception e){
            e.printStackTrace();
        }
        setDefaultThing();
        setDefaultChart();
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    void setDefaultThing() {
        tsChannel = new ThingSpeakChannel(9);
        tsChannel.setChannelFeedUpdateListener(new ThingSpeakChannel.ChannelFeedUpdateListener() {
            @Override
            public void onChannelFeedUpdated(long channelId, String channelName, ChannelFeed channelFeed) {
                // Show Channel ID and name on the Action Bar
//                Date lastUpdate = channelFeed.getChannel().getUpdatedAt();
//                Toast.makeText(getActivity(), channelFeed.getChannel().getField1().toString(), Toast.LENGTH_LONG).show();
            }
        });

        tsChannel.loadChannelFeed();
    }

    void setDefaultChart() {
        lineChartView = (LineChartView) view.findViewById(R.id.chart);
        lineChartView.setZoomEnabled(true);
        lineChartView.setValueSelectionEnabled(true);
        calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, -10);

        tsChart = new ThingSpeakLineChart(9, 2);
        tsChart.setNumberOfEntries(30);
        tsChart.setValueAxisLabelInterval(10);
        tsChart.setDateAxisLabelInterval(1);
        tsChart.useSpline(true);
        tsChart.setLineColor(Color.parseColor("#D32F2F"));
        tsChart.setAxisColor(Color.parseColor("#455a64"));
        tsChart.setChartStartDate(calendar.getTime());
        tsChart.setListener(new ThingSpeakLineChart.ChartDataUpdateListener() {
            @Override
            public void onChartDataUpdated(long channelId, int fieldId, String title, LineChartData lineChartData, Viewport maxViewport, Viewport initialViewport) {
                lineChartView.setLineChartData(lineChartData);
                lineChartView.setMaximumViewport(maxViewport);
                lineChartView.setCurrentViewport(initialViewport);
            }
        });
        tsChart.loadChartData();
    }
}
